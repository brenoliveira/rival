
function expand(menuburger){
    var nav = document.getElementById("hm");
    nav.classList.toggle("expand");
    window.addEventListener("click", function(e){
        if(!nav.contains(e.target))
            nav.classList.remove("expand");
    });
}

var introVideo = document.getElementById("introVideo");
var mutebtn = document.getElementById("mute-btn");
mutebtn.addEventListener("click", function(){
    mutebtn.classList.toggle("pulse");
    if(introVideo.muted){
        introVideo.muted = false;
        mutebtn.innerHTML = "MUTE";
    }

    else{
        introVideo.muted = true;
        mutebtn.innerHTML = "UNMUTE";
    }
}, false);

var pausebtn = document.getElementById("pause-btn");


pausebtn.addEventListener("click", function(){
    if(!introVideo.paused){
        introVideo.pause();
        pausebtn.innerHTML = "PLAY";
    }

    else{
        introVideo.play();
        pausebtn.innerHTML = "PAUSE";
    }
        
}, false);

//Object.defineProperty(HTMLMediaElement.prototype, 'playing', {
//    get: function(){
//        return !!(this.currentTime > 0 && !this.paused && !this.ended && this.readyState > 2);
//    }
//});
//
//if(document.querySelector(".bg_video").playing){ // checks if element is playing right now
//    alert("funfou");
//}

$(function() {
    $(".rslides").responsiveSlides({
  auto: true,             // Boolean: Animate automatically, true or false
  speed: 1000,            // Integer: Speed of the transition, in milliseconds
  timeout: 5000,          // Integer: Time between slide transitions, in milliseconds
  pager: true});
});

